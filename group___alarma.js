var group___alarma =
[
    [ "CERO", "group___alarma.html#ga7558fe3a1073e3ed554281ff249a4987", null ],
    [ "FINFUEGO", "group___alarma.html#gafb5654d529c89146e9e38905a4667d38", null ],
    [ "id_clave", "group___alarma.html#ga27b2f6a0c6e277eae09b49ffab58a2fb", null ],
    [ "ID_TRETARDADO", "group___alarma.html#ga9f8e3d74c75dcf729169a694732229ae", null ],
    [ "INICIOFUEGO", "group___alarma.html#ga1270cfc604c6cfd75353742c2374994b", null ],
    [ "MAXIMO_TAREAS", "group___alarma.html#gab2099ac708ee4c470468889ae5d99678", null ],
    [ "NINGUNA", "group___alarma.html#ga881407bb4d03427e2e641cc65d229da5", null ],
    [ "RETARDADOS", "group___alarma.html#ga9eccb240ee3c1355bccbba42014448e1", null ],
    [ "TRETARDADO", "group___alarma.html#gaa96707c1a980221c22915a7fe56fa713", null ],
    [ "armarCentral", "group___alarma.html#gaf5edf6087a9cbdd271c113a81050d774", null ],
    [ "armarMatrizForzado", "group___alarma.html#ga8e46d1a1d6f2cf6536a952dc39d83269", null ],
    [ "copiarMatriz", "group___alarma.html#ga2fe7e1f683efe1849f9d7d4b16e786f3", null ],
    [ "estadoActual", "group___alarma.html#gaceadf16bf67cc30643b2b7d81545d486", null ],
    [ "estadoAnterior", "group___alarma.html#ga81b1bc5103c9ecbd7f73239711ae471b", null ],
    [ "forzarCentral", "group___alarma.html#ga9fc8b553a268ab9a55cfbbb1ff1680e5", null ],
    [ "ingresoClave", "group___alarma.html#ga6130288b35fe1a769198d7bc178e923e", null ],
    [ "limpiarMatriz", "group___alarma.html#ga9e1b70f97f7fbcdde786896287f8fd22", null ],
    [ "probarIndicadores", "group___alarma.html#ga716c8ebdd90930a55f76344c2ace28d7", null ],
    [ "probarSirenas", "group___alarma.html#ga7a8950f96de6b182a6494c34514d6bd8", null ],
    [ "sensor", "group___alarma.html#gaccebf53ec27a1167a0f5e0caab061f0c", null ],
    [ "aplicacion", "group___alarma.html#ga390b7e7e64ba9748f4693d6108cf517a", null ],
    [ "manejadores", "group___alarma.html#gae65f287134443c51ece64165c2695f32", null ]
];