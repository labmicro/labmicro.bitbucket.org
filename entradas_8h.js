var entradas_8h =
[
    [ "nroSensor", "group___entradas___salidas.html#gac05e216d3cd6595cb87ac0c9a53d9795", null ],
    [ "teclaClear", "group___entradas___salidas.html#ga8ac1481b10254ece041a9ab6463aae89", null ],
    [ "teclaEstado", "group___entradas___salidas.html#ga7c2bf7a6699c9f3d43b4f6c518b0d63d", null ],
    [ "teclaF", "group___entradas___salidas.html#ga1050aa4f7e3ab0760a536510cd5a7033", null ],
    [ "teclaForzar", "group___entradas___salidas.html#ga9f96e245be403ff7b293b19e1d7f87ff", null ],
    [ "teclaPresente", "group___entradas___salidas.html#ga0068d107b9aa26e24794f589454e10ae", null ],
    [ "teclaPrueba", "group___entradas___salidas.html#gae95f0e53cbd5618fbc3bb07c1b7a30e3", null ],
    [ "teclaSirenas", "group___entradas___salidas.html#ga925dcaeab0a95f53779a4ad67ca06989", null ],
    [ "teclaUltimo", "group___entradas___salidas.html#ga192c360e911dc6910a9e72a3b21587ad", null ],
    [ "analizarMuestras", "group___entradas___salidas.html#ga1ebb85c4348696f3a0ce36f6d0915c7a", null ],
    [ "getMEL", "group___entradas___salidas.html#ga834a05d465eb28b389d8a5660247494f", null ],
    [ "getMELP", "group___entradas___salidas.html#gae57a149969157b1187b66bf6eabddcc9", null ],
    [ "getPercepcion", "group___entradas___salidas.html#gabe6f8aced7d3ea6acfd4efa2adfda6a1", null ],
    [ "inicializarEntradas", "group___entradas___salidas.html#ga21482bb302fb01daef68f60ae7defb6d", null ],
    [ "setPercepcion", "group___entradas___salidas.html#ga281452648ea83895d15649972042a95c", null ]
];