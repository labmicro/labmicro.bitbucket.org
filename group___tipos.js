var group___tipos =
[
    [ "FALSE", "group___tipos.html#gaa93f0eb578d23995850d61f7d61c55c1", null ],
    [ "TRUE", "group___tipos.html#gaa8cecfc5c5c054d2875c03e77b7be15d", null ],
    [ "BOOLEAN", "group___tipos.html#ga55feea4a69748664b608156555bb33b9", null ],
    [ "INT16", "group___tipos.html#gad87465075f24df28ef66f25e43f0bd5a", null ],
    [ "INT32", "group___tipos.html#gacb31961b5585201db715eb476f8e1d82", null ],
    [ "INT8", "group___tipos.html#ga7ebe70ceca856797319175e30bcf003d", null ],
    [ "MATRIZ", "group___tipos.html#ga5334fe3ce6d40e25c48052644651c013", null ],
    [ "UINT16", "group___tipos.html#gacfa284fa8026c4aace2728f7f15d6c13", null ],
    [ "UINT32", "group___tipos.html#ga1720f33f59b583f0c2ed071815623a86", null ],
    [ "UINT8", "group___tipos.html#gab27e9918b538ce9d8ca692479b375b6a", null ]
];