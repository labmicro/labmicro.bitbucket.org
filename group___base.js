var group___base =
[
    [ "Modulo de Entradas/Salidas", "group___entradas___salidas.html", "group___entradas___salidas" ],
    [ "Modulo Despachador de Tareas", "group___tareas.html", "group___tareas" ],
    [ "Modulo de temporizadores", "group___temporizadores.html", "group___temporizadores" ],
    [ "Tipos de datos basicos", "group___tipos.html", "group___tipos" ],
    [ "MUESTRAS", "struct_m_u_e_s_t_r_a_s.html", [
      [ "filas", "struct_m_u_e_s_t_r_a_s.html#a021bd0420dee97d0d8aeb339712caefe", null ]
    ] ],
    [ "COLA_ENTRADA", "struct_c_o_l_a___e_n_t_r_a_d_a.html", [
      [ "estado", "struct_c_o_l_a___e_n_t_r_a_d_a.html#a2a17397b151f1d43bd4028e6f2ca7a20", null ],
      [ "muestras", "struct_c_o_l_a___e_n_t_r_a_d_a.html#a2147297fd7b34f2f23b1db8cacb7690e", null ]
    ] ],
    [ "ESTADO_MUESTRAS", "group___base.html#ga0cfe5bbfa4ab0c6f42af36134a82a285", null ],
    [ "inicializarMuestras", "group___base.html#ga13b413375095ee9ccae915b706881028", null ],
    [ "tomarMuestra", "group___base.html#gaf55c7eb305c59b585133d8b8cf33f644", null ],
    [ "colaEntrada", "group___base.html#gad50add93c9663aefdf3d1ea0bb885ea3", null ]
];