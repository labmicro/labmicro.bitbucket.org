var diagramas =
[
    [ "Diagramas de Secuencia", "secuencias.html", [
      [ "Armado Presente", "secuencias.html#armarCentral", null ],
      [ "Mostrar Estado Actual", "secuencias.html#estadoActual", null ],
      [ "Prueba de Indicadores", "secuencias.html#probarIndicadores", null ],
      [ "Prueba de Sirenas", "secuencias.html#probarSirenas", null ],
      [ "Sensor Instantaneo", "secuencias.html#sensorInstantaneo", null ],
      [ "Sensor Permanente", "secuencias.html#sensorPermanente", null ],
      [ "Sensor Retardado", "secuencias.html#sensorRetardado", null ]
    ] ],
    [ "Diagramas de Actividades", "actividades.html", [
      [ "Activacion de sensor", "actividades.html#sensor", null ]
    ] ]
];