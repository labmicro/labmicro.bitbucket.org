var salidas_8h =
[
    [ "ledApagado", "group___entradas___salidas.html#ga65bbdef18e35e7aee874cfdbe600d5df", null ],
    [ "ledArmadoPrincipal", "group___entradas___salidas.html#ga7f58698729c3dee9ecbe52c63a022ddd", null ],
    [ "ledArmadoRemoto", "group___entradas___salidas.html#ga698a32d6f0f16795bad9ead0fee51ad2", null ],
    [ "ledFallaPrincipal", "group___entradas___salidas.html#ga16e1f48f4f5161491a81c160a3974ebd", null ],
    [ "ledFallaRemoto", "group___entradas___salidas.html#ga20998c32e05fe4dd118e8d7e72fa7cd7", null ],
    [ "ledForzadoPrincipal", "group___entradas___salidas.html#ga5d0f4a916aa909041a09879019ff2498", null ],
    [ "ledForzadoRemoto", "group___entradas___salidas.html#ga5348b43e1abaef9d0d4747fc06946a2e", null ],
    [ "ledInactivoPrincipal", "group___entradas___salidas.html#gac58adcb5e3b3b99173e28e60ac44a952", null ],
    [ "ledIntrusionPrincipal", "group___entradas___salidas.html#ga8bd86243583f2cefc19b4bd727478ad6", null ],
    [ "ledPrendido", "group___entradas___salidas.html#gaa48400b6dd3342c91928674789593a4a", null ],
    [ "ledPresentePrincipal", "group___entradas___salidas.html#ga6b5bcd697d790b7b04320ca9adf5d5fe", null ],
    [ "ledPresenteRemoto", "group___entradas___salidas.html#ga87c0df5c22b92cab57831dd50140bf3f", null ],
    [ "ledTitilando", "group___entradas___salidas.html#ga8269d56e3fda342d4ae5094178d849dd", null ],
    [ "SALIDAS", "group___entradas___salidas.html#ga09f768b0444c63860913402f9c51efb5", null ],
    [ "getLed", "group___entradas___salidas.html#ga36214e46391b7e43e25096ea2ab71abd", null ],
    [ "getSalidas", "group___entradas___salidas.html#ga81876f6cf50a0ba5cc8ee3d2c531d47a", null ],
    [ "inicializarSalidas", "group___entradas___salidas.html#gab8226e8410223cfde0fcfc431756b688", null ],
    [ "refrescarSalidas", "group___entradas___salidas.html#gac97b29ac13e68713d27465daa3a2ee3e", null ],
    [ "setLed", "group___entradas___salidas.html#ga5f5b07824308fa9c3eeacf69259f714f", null ],
    [ "setSalidas", "group___entradas___salidas.html#gae3e3502b1968935954a5374047633fd9", null ],
    [ "setSirena", "group___entradas___salidas.html#ga9eaa1d8d084f017915a7e7db26d0ba19", null ]
];