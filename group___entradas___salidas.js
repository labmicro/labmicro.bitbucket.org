var group___entradas___salidas =
[
    [ "SALIDAS_STRUCT", "struct_s_a_l_i_d_a_s___s_t_r_u_c_t.html", [
      [ "filas", "struct_s_a_l_i_d_a_s___s_t_r_u_c_t.html#a021bd0420dee97d0d8aeb339712caefe", null ]
    ] ],
    [ "ledApagado", "group___entradas___salidas.html#ga65bbdef18e35e7aee874cfdbe600d5df", null ],
    [ "nroSensor", "group___entradas___salidas.html#gac05e216d3cd6595cb87ac0c9a53d9795", null ],
    [ "SALIDAS", "group___entradas___salidas.html#ga09f768b0444c63860913402f9c51efb5", null ],
    [ "analizarMuestras", "group___entradas___salidas.html#ga1ebb85c4348696f3a0ce36f6d0915c7a", null ],
    [ "getLed", "group___entradas___salidas.html#ga36214e46391b7e43e25096ea2ab71abd", null ],
    [ "getMEL", "group___entradas___salidas.html#ga834a05d465eb28b389d8a5660247494f", null ],
    [ "getMELP", "group___entradas___salidas.html#gae57a149969157b1187b66bf6eabddcc9", null ],
    [ "getPercepcion", "group___entradas___salidas.html#gabe6f8aced7d3ea6acfd4efa2adfda6a1", null ],
    [ "getSalidas", "group___entradas___salidas.html#ga81876f6cf50a0ba5cc8ee3d2c531d47a", null ],
    [ "inicializarEntradas", "group___entradas___salidas.html#ga21482bb302fb01daef68f60ae7defb6d", null ],
    [ "inicializarSalidas", "group___entradas___salidas.html#gab8226e8410223cfde0fcfc431756b688", null ],
    [ "refrescarSalidas", "group___entradas___salidas.html#gac97b29ac13e68713d27465daa3a2ee3e", null ],
    [ "setLed", "group___entradas___salidas.html#ga5f5b07824308fa9c3eeacf69259f714f", null ],
    [ "setPercepcion", "group___entradas___salidas.html#ga281452648ea83895d15649972042a95c", null ],
    [ "setSalidas", "group___entradas___salidas.html#gae3e3502b1968935954a5374047633fd9", null ],
    [ "setSirena", "group___entradas___salidas.html#ga9eaa1d8d084f017915a7e7db26d0ba19", null ]
];